## GitLab ssh Set-up

 A public gitlab project to simplify the setting-up of and use of ssh keys to access a GitLab Remote Repo from the Git command line on a local development machine.

## Description

Creates ssh authorisation key for gitlab access. The public key is presented for pasting into the GitLab ssh key screen, and the private key is installed in `$HOME/.ssh`. The script also creates an entry in `$HOME/.ssh/config` to avoid having to specify the ssh key [explicitly](https://superuser.com/questions/232373/how-to-tell-git-which-private-key-to-use) on the Git CLI. 

## Pre-Requisites

- An account on GitLab.com or equivalent self-hosted gitlab instance.  
- `git` is installed.

## Instructions (Commands Issued from the Local Development Machine Unless Otherwise Stated)
1. Logon to your gitlab instance and go to the screen that sets-up ssh keys (e.g. `https://gitlab.com/-/profile/keys`) - substitute own domain name if required.

2. Open up a konsole window to get to the Bash prompt, and changed to the directory you want to create the local Git Repo in. 

3. Clone the repo, either:

      `git clone https://gitlab.com/Moortec-IaC/bashscripts/gitlab-ssh-setup.git # or`

      `git clone git@gitlab.com:Moortec-IaC/bashscripts/gitlab-ssh-setup.git # Needs to have an ssh key set-up 1st` 

4. `chmod u+x gitlab-ssh-setup/gitlab-ssh-setup.sh`

5. `gitlab-ssh-setup/gitlab-ssh-setup.sh`

6. Follow the instructions in the script file. Do not use spaces in any answers, and change the default [gitlab.com] for your own self-hosted domain if required, and at the appropriate point copy-and-paste the ssh public key from the Konsole screen to the gitlab ssh screen accessed in step 1. 

And that is it, from now on you should be able to access all your projects in gitlab using git commands.

7. Delete the set-up script if no plans to re-use or change: `rm -r gitlab-ssh-setup -f`

## Test

To test that the ssh keys have been properly set-up:
```
ssh -T git@gitlab.com
```
Which should respond with something like the following:
```
Welcome to Gitlab, @myusername
```
If $HOME/.ssh/config has not been set-up to assoicate the keyfile with gitlab.com then the key can be explicitly identified as shown:
```
ssh -i keyfilename_id_rsa -T git@gitlab.com
```

### Example ssh config file
```
Host gitlab.com
  HostName gitlab.com
  IdentityFile /home/use/.ssh/keyfilename_id_rsa
  User git
```

## Amazon Linux / CentOS

If get a bad owner or bad permissions error then `chmod 600 ~/.ssh/config`
