#!/usr/bin/env bash
#++
#  FACILITY:
#
#    Moortec-IaC - Gitlab Access Set-Up Script
#
#  MODULE DESCRIPTION:
#
#      This module configures access to gitlab
#
#  AUTHORS:
#
#      Peter Burke.
#
#  CREATION DATE:
#
#      7-MAY-2020
#
#  ENVIRONMENT:
#
#      Compiler/Language:  Bash Script
#      Target system:      Any Linux
#      Comments:           
#sya
#  DESIGN NOTES:
#
#      Collects user config at the beginning. 
#
#  TEST STATUS AND HISTORY:
#
#       Testing on RHEL 7 Gold AMI
#
#  Version     Date        Performed by
#  -------     ----        ------------
#
#  0.1         7-May-20    Peter K. Burke
#
#  0.2         24-Nov-20   Peter K. Burke
#
#  Tested 'happy path'.
#
#  MODIFICATION HISTORY (CHANGE LOG):
#
#  Version     Date        Author          Change request/review
#  -------     ----        ------          ---------------------
#
#  0.1          7-May-20   Peter K. Burke  Initial Version. Incomplete
#                                          exeption handling.
#
#  0.2          23-Nov-20   Peter K. Burke 
#                                          
#  0.3          14-Apr-21   Peter K. Burke Improved Instructions.
#                                          
# --
#

# Constants
readonly default_gitlab="gitlab.com"

# Informative Error Handling
export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#set -x # Uncomment for Debugging

# Get the parameters required for set-up
echo "+=======================================================================+"
echo "| Gitlab Setup script                                                   |"
echo "|                                                                       |"
echo "| Configures GitLab access via ssh keys.                                |"
echo "|                                                                       |"
echo "| Hints are in braces (), default values in square brackets []          |"
echo "+=======================================================================+"
read -p "Enter the gitlab domain name [$default_gitlab]: " gitlab_domain 
  gitlab_domain=${gitlab_domain:-$default_gitlab}
read -p 'Enter your Gitlab Username \(lastname.firstname): ' gitlab_user
read -p 'Enter descriptive name for keys (e.g. projectX-gitlab.com): ' app_env

keyname="${gitlab_user}-${gitlab_domain}_id_rsa"

# Create the keys for Accessing Gitlab
echo '------------------------------------------------------------------------------'
echo 'Creating Gitlab keys.                                                         '
echo ' copy-and-paste the displayed key into your gitlab account                    '
echo ' at https://gitlab.com/-/profile/keys                                         ' 
echo 'Start the copy as the line beginning ssh-rsa, end before the dotted line.     '
echo 'User>>Settings>>ssh keys then paste the key and click Add Key                 '
echo '------------------------------------------------------------------------------'
# -C sets the comment in the file, -f is the output file, -q stops the picture
ssh-keygen \
  -t rsa \
  -b 4096 \
  -C "$gitlab_user@$app_env" \
  -f "$HOME/.ssh/${keyname}" \
  -q \
  -N ""
cat "$HOME/.ssh/${keyname}.pub"
echo '------------------------------------------------------------------------------'
read -p "Hit Return to Carry on, when pasted public key into GitLab"
set -x
ssh -i "$HOME/.ssh/${keyname}" -T "git@${gitlab_domain}"

if [ $? -ne 0 ]; then
   echo "GitLab Set-Up Failed. Terminating"
   exit 1
fi

# Create the ssh config associated with GitLab using a here doc.
cat <<EOF >> $HOME/.ssh/config
Host ${gitlab_domain}
  HostName ${gitlab_domain}
  IdentityFile $HOME/.ssh/${keyname}
  User git
EOF

exit 0 # Successful exit
